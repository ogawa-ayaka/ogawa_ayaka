/**
 *
 */
package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

/**
 * @author ogawa.ayaka
 *
 */
@WebFilter({"/user_managements","/user_settings" , "/signup"})

public class ManagementFilter implements Filter{

	public static String INIT_PARAMETER_NAME_ENCODIDNG = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rep = (HttpServletResponse) response;

		HttpSession session = req.getSession();

		User user = (User) session.getAttribute("loginUser");
		int branch_id = user.getBranch_id();
		int department_id = user.getDepartment_id();


		if(branch_id != 1 && department_id != 1) {

			rep.sendRedirect("./");

			return;




		}





		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {


	}




}
