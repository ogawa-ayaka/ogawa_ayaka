/**
 *
 */
package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

/**
 * @author ogawa.ayaka
 *
 */

@WebFilter("/*")

public class LoginFilter implements Filter{

	public static String INIT_PARAMETER_NAME_ENCODIDNG = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";





	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{

		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse rep = (HttpServletResponse)response;


		//System.out.println(req.getServletPath());

		String loginPage = "/login";
		String servletPath = req.getServletPath();

		HttpSession session = req.getSession();

		User user =(User) session.getAttribute("loginUser");


		if(!loginPage.equals(servletPath) && user == null){

			rep.sendRedirect("login");

			return;

		}

		chain.doFilter(request, response);
	}



	@Override
	public void init(FilterConfig config) {

	}
	@Override
	public void destroy() {

	}

}
