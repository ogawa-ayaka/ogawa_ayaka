/**
 *
 */
package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;



/**
 * @author ogawa.ayaka
 *
 */
public class BranchService {

	private static final int LIMIT_NUM = 1000;
	public List<Branch> getBranchlist(){

		Connection connection = null;

		try {

			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranches(connection,LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
	        throw e;

		}finally {
			close(connection);

		}


	}

}
