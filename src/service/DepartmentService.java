/**
 *
 */
package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;



/**
 * @author ogawa.ayaka
 *
 */
public class DepartmentService {
	private static final int LIMIT_NUM = 1000;

	public List<Department> getDepartmentlist(){

		Connection connection = null;

		try {

			connection = getConnection();

			DepartmentDao departmentDao = new DepartmentDao();
			List<Department> ret = departmentDao.getDepartments(connection,LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
	        throw e;

		}finally {
			close(connection);

		}


	}


}
