/**
 *
 */
package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

/**
 * @author ogawa.ayaka
 *
 */
public class UserService {


	public User accountConfilm(String account) {

		Connection connection = null;

		try {


			connection = getConnection();

			UserDao userDao = new UserDao();

			User user = userDao.getaccountConfilm(connection, account);

			commit(connection);

			 return user;


		}catch(RuntimeException e) {
			rollback(connection);
			throw e;


		}finally {
			close(connection);
		}

	}








	public void register(User user) {
		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw(e);
		}catch(Error e) {
			rollback(connection);
			throw(e);
		}finally {
			close(connection);
		}
	}


	private static final int LIMIT_NUM = 1000;
	public List<User> getUserslist(){

		Connection connection = null;

		try {

			connection = getConnection();

			UserDao usersListDao = new UserDao();
			List<User> ret = usersListDao .getUsersList(connection, LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public User getUser(int userId){
		Connection connection = null;
		try {

			connection = getConnection();

			 UserDao userDao = new UserDao();
			 User user = userDao.getUser(connection, userId);

			 commit(connection);

			 return user;


		}catch(RuntimeException e) {
			rollback(connection);
			throw e;


		}finally {
			close(connection);
		}

	}

	public void update(User user) {

		Connection connection = null;
		try {

			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public void stopped(User switchUser) {

		Connection connection = null;
		try {

			connection = getConnection();
			UserDao userDao = new UserDao();
			userDao.stopped(connection, switchUser);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {

			close(connection);
		}




	}

}
