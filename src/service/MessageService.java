/**
 *
 */
package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;


/**
 * @author ogawa.ayaka
 *
 */
public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e ;
		}catch (Error e) {
			rollback(connection);
			throw e ;
		}finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;
	public List<UserMessage> getMessage(String startDate, String endDate, String searchStr){


		 Connection connection = null;





		try {
			connection = getConnection();

			String start = null;

			String end = null;

			String search = searchStr;

			if(StringUtils.isEmpty(startDate) == false) {

				start = startDate + " 00:00:00";




			}else {

				start = "2019-01-01 00:00:00";
			}



			if(StringUtils.isEmpty(endDate) == false) {

				end = endDate + " 23:59:59";


			}else {

				Calendar cl = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				end = (sdf.format(cl.getTime()));
			}



			if(StringUtils.isEmpty(searchStr) == false) {
				search = searchStr;

			}else {

				search = "%";
			}

			//System.out.println(start);
			//System.out.println(end);
			//System.out.println(search);





			UserMessageDao messageDao  = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection,LIMIT_NUM , start , end, search);


			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}


	public void delete(Message dltmessage) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, dltmessage);
			commit(connection);


		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}

	}




}
