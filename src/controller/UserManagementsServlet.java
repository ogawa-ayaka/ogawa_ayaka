/**
 *
 */
package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * @author ogawa.ayaka
 *
 */
@WebServlet(urlPatterns = {"/user_managements"})
public class UserManagementsServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{

		List<User> users =  new UserService().getUserslist();

		request.setAttribute("users", users);

		List<Branch> branches = new BranchService().getBranchlist();

		//System.out.println(branches.get(0).getBranch_name());

		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().getDepartmentlist();
		request.setAttribute("departments", departments);


		request.getRequestDispatcher("/user_management.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException{


		User switchUser = new User();
		switchUser.setId(Integer.parseInt(request.getParameter("id")));
		switchUser.setIs_stopped(Integer.parseInt(request.getParameter("is_stopped")));

		//System.out.println(switchUser.getIs_stopped());

		new UserService().stopped(switchUser);

		response.sendRedirect("user_managements");



	}




}
