/**
 *
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * @author ogawa.ayaka
 *
 */
@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{

		List<Branch> branches = new BranchService().getBranchlist();
		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().getDepartmentlist();
		request.setAttribute("departments", departments);


		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{


		List<String> messages = new ArrayList<String>();


		HttpSession session = request.getSession();

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

		String account =user.getAccount();
		User accountCnfilm = new UserService().accountConfilm(account);


		if(isValid(request, messages, user) == true) {



			new UserService().register(user);

			response.sendRedirect("user_managements");

		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}

	}


	private boolean isValid(HttpServletRequest request, List<String> messages, User user) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		//int department_id = Integer.parseInt(request.getParameter("department_id"));

		int branch_id = user.getBranch_id();
		int department_id = user.getDepartment_id();

		if (StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			 messages.add("パスワードを入力してください");
		}

		if((branch_id == 1) &&( department_id >= 3)){
			messages.add("支店名と部署・役職名が不一致です");
		}

		if((branch_id != 1)&& (department_id <= 2)) {
			messages.add("支店名と部署・役職名が不一致です");
		}



		if (messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
