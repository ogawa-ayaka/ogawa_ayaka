/**
 *
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * @author ogawa.ayaka
 *
 */

@WebServlet(urlPatterns = {"/user_settings"})
public class UserSettingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	List<Branch> branches = new BranchService().getBranchlist();
	List<Department> departments = new DepartmentService().getDepartmentlist();

	@Override

	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException{


		int id = Integer.parseInt(request.getParameter("id"));
		User editUser = new UserService().getUser(id);

		request.setAttribute("editUser", editUser);


		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("user_settings.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException{

		List<String> messages = new ArrayList<String>();


		HttpSession session = request.getSession();


		User editUser = getEditUser(request);

		//String account = request.getParameter("account");
	//	User accountCnfilm = new UserService().getUser(account);

		if(isValid(request, messages, editUser)) {

			try {

				new UserService().update(editUser);

			}catch(NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("user_settings.jsp").forward(request, response);
                return;

			}
			 session.setAttribute("id", editUser);
			 response.sendRedirect("user_managements");

		}else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("user_settings.jsp").forward(request, response);


		}

	}

	private User getEditUser(HttpServletRequest request)throws IOException, ServletException{

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User editUser){

		String account = request.getParameter("account");
        String password = request.getParameter("password");
        String passwordConfirm =request.getParameter("passwordConfirm");
        int branch_id = editUser.getBranch_id();
        int department_id = editUser.getDepartment_id();



        if (StringUtils.isEmpty(account) == true) {
            messages.add("アカウント名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if(!(password.equals( passwordConfirm)) ) {
        	messages.add("パスワードと確認用パスワードが一致しません");

        }


        if(!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}$")) {


        	messages.add("パスワードは半角英数字記号かつ6～20文字で入力してください");
        }

         if(!(account.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}$")) ) {
        	 messages.add("アカウントは半角英数字記号かつ6～20文字で入力してください");

         }

        if(branch_id == 1 && department_id >= 3) {
        	messages.add("支店名と部署・役職名が不一致です");
        }

        if(branch_id != 1 && department_id <= 2) {
        	messages.add("支店名と部署・役職名が不一致です");
        }

        if (messages.size() == 0) {
            return true;
        }else {
        	return false;
        }


	}



}


