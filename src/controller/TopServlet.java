/**
 *
 */
package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;



/**
 * @author ogawa.ayaka
 *
 */

@WebServlet(urlPatterns = {"/index.jsp"})
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {



		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String search = request.getParameter("search");

		//System.out.println(start);
		//System.out.println(end);
		//System.out.println(search);

		List<UserMessage> messages = new MessageService().getMessage(start, end,search);

		request.setAttribute("messages", messages);


		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("comments", comments);

		HttpSession session = request.getSession();
		User loginUser =(User) session.getAttribute("loginUser");

		request.setAttribute("loginUser", loginUser);







		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}

}
