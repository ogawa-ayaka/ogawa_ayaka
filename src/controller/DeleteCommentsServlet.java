/**
 *
 */
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentService;

/**
 * @author ogawa.ayaka
 *
 */

@WebServlet(urlPatterns = {"/deletecomments"})
public class DeleteCommentsServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{

		Comment comment = new Comment();
		comment.setId(Integer.parseInt(request.getParameter("comment_id")));


		new CommentService().delete(comment);

		response.sendRedirect("./");

	}

}
