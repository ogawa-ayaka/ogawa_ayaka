/**
 *
 */
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;
//import service.MessageService;

/**
 * @author ogawa.ayaka
 *
 */

@WebServlet(urlPatterns = {"/deletemessages"})
public class DeleteMessageServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{

		//HttpSession session = request.getSession();

		//User user = (User) session.getAttribute("loginUser");

		Message message = new Message();

		message.setId(Integer.parseInt(request.getParameter("message_id")));
		//message.setUserId(user.getId());


		new MessageService().delete(message);

		response.sendRedirect("./");

	}




}
