/**
 *
 */
package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ogawa.ayaka
 *
 */
public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	public int getId(){
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	private int message_Id;
	public int getMessageId() {
		return message_Id;
	}
	public void setMessageId(int message_Id) {
		this.message_Id = message_Id;
	}

	private String comment;
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	private Date created_date;
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	private Date updated_Date;
	public Date getUpdated_Date() {
		return updated_Date;
	}
	public void setUpdated_Date(Date updatedDate) {
		this.updated_Date = updatedDate;
	}


}
