/**
 *
 */
package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ogawa.ayaka
 *
 */
public class Message implements Serializable{
	private static final long serialVersionUID = 1L;


	private int id;
	public int getId(){
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private String category;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;

		}

	private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	private Date created_Date;
	public Date getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(Date createdDate) {
		this.created_Date = createdDate;
	}


	private Date updated_Date;
	public Date getUpdated_Date() {
		return updated_Date;
	}
	public void setUpdated_Date(Date updatedDate) {
		this.updated_Date = updatedDate;
	}



}
