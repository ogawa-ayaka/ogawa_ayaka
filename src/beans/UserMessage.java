/**
 *
 */
package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ogawa.ayaka
 *
 */
public class UserMessage implements Serializable{
	private static final long serialVersionUID = 1L;


	private int id;
	public int getId(){
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	private String account;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private String category;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	private Date created_date;
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	private Date updated_Date;
	public Date getUpdated_date() {
		return updated_Date;
	}
	public void setUpdated_date(Date updatedDate) {
		this.updated_Date = updatedDate;
	}
}
