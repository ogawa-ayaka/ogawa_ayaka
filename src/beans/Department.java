/**
 *
 */
package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ogawa.ayaka
 *
 */
public class Department implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private String department_name;
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	private Date createdDate;
	public Date getCreatdeDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	private Date updatedDate;
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}























}
