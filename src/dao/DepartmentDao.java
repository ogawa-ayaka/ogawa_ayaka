/**
 *
 */
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;


/**
 * @author ogawa.ayaka
 *
 */
public class DepartmentDao {

	public List <Department> getDepartments(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("departments.id as id, ");
			sql.append("departments.department_name as name, ");
			sql.append("departments.created_date as created_date ");
			sql.append("FROM departments ");
			sql.append("ORDER BY created_date DESC limit " + num);


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List <Department> ret = toDepartmentList(rs);
			return ret;



		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	private List<Department> toDepartmentList(ResultSet rs)throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String department_name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");


				Department department = new Department();
				department.setId(id);
				department.setDepartment_name(department_name);
				department.setCreatedDate(createdDate);

				ret.add(department);

			}

			return ret;
		}finally {
			close(rs);
		}
	}





}
