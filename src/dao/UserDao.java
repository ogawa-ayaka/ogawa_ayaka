/**
 *
 */
package dao;


import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
/**
 * @author ogawa.ayaka
 *
 */
public class UserDao {

	public void insert(Connection connection, User user) {



		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_stopped");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // department_id
			sql.append(", ?"); // is_stopped   ここはデフフォルトで0？
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");


			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getDepartment_id());
			ps.setInt(6, user.getIs_stopped());
			ps.executeUpdate();




		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	public User getUser(Connection connection, String account, String password) {


		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?  AND password = ? AND is_stopped = 0 ";

			 ps = connection.prepareStatement(sql);
			 ps.setString(1, account);
			 ps.setString(2, password);

			 System.out.println(ps);

			 ResultSet rs = ps.executeQuery();
			 List<User> userList = toUserList(rs);
			 if(userList.isEmpty() == true) {
				 return null;

			 }else if(2 <= userList.size()) {
				 throw new IllegalStateException("2 <= userList.size()");
			 }else {
				 return userList.get(0);
			 }


		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	private List<User> toUserList(ResultSet rs) throws SQLException{
		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branch_id = rs.getInt("branch_id");
				int department_id = rs.getInt("department_id");
				byte is_stopped = rs.getByte("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");



				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setPassword(password);
				user.setBranch_id(branch_id);
				user.setDepartment_id(department_id);
				user.setIs_stopped(is_stopped);;
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);


				ret.add(user);




			}return ret;
		}finally {
			close(rs);
		}

	}

	public List<User> getUsersList (Connection connection , int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.department_id as department_id, ");
			sql.append("users.is_stopped as is_stopped, ");
			 sql.append("users.created_date as created_date ");
			sql.append("FROM users ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsersList(rs);
			return ret;


		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}

	}


	private List<User> toUsersList(ResultSet rs)throws SQLException{

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {

				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int department_id = rs.getInt("department_id");
				byte is_stopped = rs.getByte("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");

				User users = new User();
				users.setId(id);
				users.setAccount(account);
				users.setName(name);
				users.setBranch_id(branch_id);
				users.setDepartment_id(department_id);
				users.setIs_stopped(is_stopped);
				users.setCreatedDate(createdDate);


				ret.add(users);

			}
			return ret;

		}finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;

			}else if (2 <= userList.size()){
				 throw new IllegalStateException("2 <= userList.size()");

			}else {
				return userList.get(0);

			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
		       close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			 ps.setString(1, user.getAccount());
			 ps.setString(2, user.getName());
			 ps.setString(3, user.getPassword());
			 ps.setInt(4, user.getBranch_id());
			 ps.setInt(5, user.getDepartment_id());
			 ps.setInt(6, user.getId());

			 int count = ps.executeUpdate();



			 if (count == 0) {
	                throw new NoRowsUpdatedRuntimeException();
	            }

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	public void stopped(Connection connection, User switchUser) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_stopped = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,  switchUser.getIs_stopped());
			ps.setInt(2, switchUser.getId());


			//System.out.println(ps);

			int count = ps.executeUpdate();


			if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }


		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}


	public User getaccountConfilm(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, account);

			ResultSet accountRs = ps.executeQuery();
			System.out.println(accountRs);


			List<User> userList = toUserList(accountRs);
			if(userList.isEmpty() == true) {
				return null;

			}else if(2 <= userList.size()) {
			 throw new IllegalStateException("2 <= userList.size()");
			}else {
			 return userList.get(0);
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
		       close(ps);
		}
	}



}


