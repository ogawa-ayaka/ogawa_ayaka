/**
 *
 */
package dao;


import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

/**
 * @author ogawa.ayaka
 *
 */
public class MessageDao {

	public void insert (Connection connection, Message message) {






		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append(" user_id");
			sql.append(", title");
			sql.append(", message");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // title
			sql.append(", ?"); // message
			sql.append(" ,?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());


			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getMessage());
			ps.setString(4, message.getCategory());



			ps.executeUpdate();



		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void delete (Connection connection, Message dltmessage) {

		PreparedStatement ps = null;

		try {
			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, dltmessage.getId());

			ps.executeUpdate();



		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}









}


