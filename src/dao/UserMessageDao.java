/**
 *
 */
package dao;


import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

/**
 * @author ogawa.ayaka
 *
 */
public class UserMessageDao {

	public List<UserMessage> getUserMessages (Connection connection, int num, String start, String end, String search){


		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.message as message, ");
	        sql.append("messages.user_id as user_id, ");
	        sql.append("messages.category as category, ");
	        sql.append("messages.title as title, ");
	        sql.append("users.account as account, ");
	        sql.append("users.name as name, ");
	        sql.append("messages.created_date as created_date ");
	        sql.append("FROM messages ");


	        sql.append("INNER JOIN users ");
	        sql.append("ON messages.user_id = users.id ");

	        sql.append("WHERE messages.created_date ");
	        sql.append("BETWEEN ? AND ? ");
	        sql.append("AND category LIKE ? ");
	        sql.append("ORDER BY created_date DESC limit " + num);


	        //System.out.println(start);
			//System.out.println(end);


	        ps = connection.prepareStatement(sql.toString());
			ps.setString(1, start);
			ps.setString(2, end);
			ps.setString(3, search);
			//System.out.println(ps);


	        ResultSet rs = ps.executeQuery();
	        List<UserMessage> ret = toUserMessageList(rs);
	        return ret;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException{


		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while(rs.next()) {

				int id = rs.getInt("id");
				String message = rs.getString("message");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				String title = rs.getString("title");
				String account = rs.getString("account");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");



				UserMessage userMessage = new UserMessage();
				userMessage.setId(id);
				userMessage.setMessage(message);
				userMessage.setUserId(userId);
				userMessage.setTitle(title);
				userMessage.setCategory(category);
				userMessage.setAccount(account);
				userMessage.setName(name);
				userMessage.setCreated_date(createdDate);

				ret.add(userMessage);



			}
			return ret;
		}finally {
			close(rs);
		}
	}




}
