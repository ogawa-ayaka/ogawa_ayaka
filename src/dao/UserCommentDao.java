/**
 *
 */
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

/**
 * @author ogawa.ayaka
 *
 */
public class UserCommentDao {


	public List<UserComment> getUserComments(Connection connection, int num){


		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.comment as comment, ");
			sql.append("comments.message_id as message_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			 ResultSet rs = ps.executeQuery();

			 List<UserComment> ret = toUserCommentList(rs);
			 return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	private List<UserComment> toUserCommentList(ResultSet rs)throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				int message_id = rs.getInt("message_id");
				String comment = rs.getString("comment");

				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment userComment = new UserComment();
				userComment.setId(id);
				userComment.setUserId(user_id);
				userComment.setMessageId(message_id);
				userComment.setComment(comment);
				userComment.setName(name);
				userComment.setCreated_date(createdDate);


				 ret.add(userComment);


			}
			return ret;
		}finally {
			close(rs);
		}
	}

}
