/**
 *
 */
package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

/**
 * @author ogawa.ayaka
 *
 */
public class BranchDao {

	public List <Branch> getBranches(Connection connection, int num){


		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("branches.id as id, ");
			sql.append("branches.branch_name as name, ");
			sql.append("branches.created_date as created_date ");
			sql.append("FROM branches ");
			sql.append("ORDER BY created_date DESC limit " + num);


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List <Branch> ret = toBranchList(rs);
			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	private List<Branch> toBranchList(ResultSet rs)throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String branch_name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");


				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranch_name(branch_name);
				branch.setCreatedDate(createdDate);

				ret.add(branch);

			}

			return ret;
		}finally {
			close(rs);
		}
	}

}
