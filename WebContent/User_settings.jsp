<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>編集</title>
	</head>
	<body>

	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">

			<div class = "errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>

				</ul>
			</div>
			<c:remove var = "errorMessages" scope = "session"/>

		</c:if>

		<form action = "user_settings" method = "post">	<br/>
			<input name="id" value="${editUser.id}" id="id" type="hidden"/>
			<label for="name">名前</label>
			<input name="name" value="${editUser.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />

			<label for="account">アカウント名</label>
			<input name="account" value="${editUser.account}" /><br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" required><br />

			<label for="passwordConfirm">パスワード（確認）:</label>
			<input type="password" name="passwordConfirm" id="passwordConfirm" required><br>


			<div class = "branches">支店名
			<select name = "branch_id">
				<c:forEach items = "${branches}" var= "branch">
					<option value = "${ branch.id}"> ${branch.branch_name} </option>
				</c:forEach>
			</select>
			</div>

			<div class = "departments">部署・役職名
			<select name = "department_id">
				<c:forEach items = "${departments}" var = "department">
					<option value = "${department.id}">${department.department_name}</option>
				</c:forEach>
			</select>
			</div>
			<input type="submit" value="登録" /> <br />
		</form>


		 <a href="user_managements">戻る</a>

	<div class = "copyright">Copyright(c)ogawa ayaka</div>
	</div>
	</body>
</html>