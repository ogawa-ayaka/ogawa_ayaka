<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
		<title>	新規投稿画面</title>
	</head>
	<body>

	＜＜新規投稿画面＞＞
	<br/>
		<div class = "mamin-contents">

			<form action="messages" method="post">

			<label for = "title" >件名</label>
			<input name = "title" id = "title"/><br/>

			<label for = "category">カテゴリー</label>
			<input name = "category" id = "category"><br/>



			新規投稿<br/>
			<textarea name="message" cols="100" rows="5" class="tweet-box" id = "message"></textarea>

			<br/>
			<input type="submit" value="投稿" >（1000文字まで）

			<a href = "./">戻る</a>

			</form>
			<div class = "copyright">Copyright(c)ogawa ayaka</div>
		</div>

</body>
</html>