<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
  		<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
		<title>ホーム画面</title>
	</head>
	<body>

	＜＜ホーム画面＞＞

	<div class = "main-contents">
			<br/><div class = "header">
			<!-- <a href = "signup">ユーザー新規登録</a>-->
			<a href = "user_managements">ユーザー管理</a>
			<a href = "messages">新規投稿</a>
			<a href="logout">ログアウト</a>
			</div>
		</div>
		<br/><c:set value = "${loginUser}" var = "loginUser"/>
		ログインユーザー名：<c:out value = "${loginUser.name}"/><br/>



		<br/>
			<div class = "date">
		<form action = "index.jsp" method = "get">
			日付検索：<input type = "date" name = "start" value = "${start}" >
			～ <input type = "date" name = "end" value ="${end}">
				<!--  <input type="submit" value="検索">-->
			<!-- /form-->
			</div>

			<br/>
			<div class = "search">
			<!--  form action = "index.jsp" method = "get"-->
			カテゴリー検索:<input type = "text" name = "search" value = "${search }">
			<input type = "submit" value = "検索">
		</form>
		</div>

		<div class = "messages">
			<c:forEach items="${messages}" var="message">
				<div class = "message">
					<br/>名前<div class="name"><c:out value="${message.name}" /></div>
					<br/>件名<div class = "title"><c:out value="${message.title}" /></div>
					<br/>カテゴリー<div class = "category"><c:out value="${message.category}" /></div>
					<br/>投稿文<div class = "message"><c:out value = "${message.message}"/></div>
					<br/>投稿日時<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>


					<c:set value = "${loginUser}" var = "loginUser"/>
					<c:if test = "${loginUser.id == message.userId}">
						<div class = "deletemessages">
							<form action = "deletemessages " method = "post">
								<input type = "hidden" name = "message_id" value = "${message.id}">
								<input type = "submit" value ="投稿削除">(上記の内容を削除します)
							</form>
						</div>
					</c:if>
					<br>
				</div>


				<div class = "new-commenst">
				<form action = "comments" method = "post">
					コメント<br/>
					<textarea name="comment" cols="100" rows="5" class="tweet-box" id = "comment"></textarea>
					<input type = "hidden" name = "message_id" id = "message_id " value = "${message.id}">
					<input type="submit" value="コメント" >（500文字まで）
				</form>
				</div>

				<br/><div class = "comments">
					<c:forEach items = "${comments}" var= "comment">
						<c:if test = "${message.id == comment.messageId}">
							<div class = "name">名前：<c:out value = "${comment.name}"/>
							</div>
							<div class="comment">コメント内容：<c:out value="${comment.comment}" /></div>
							<div class="date">投稿日時：<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

								<div class = "deletecomments">
									<form action = "deletecomments" method = "post">
									<c:set value = "${loginUser}" var = "loginUser"/>
									<c:if test = "${loginUser.id == comment.userId}">
										<input type = "hidden" name = "comment_id" value = "${comment.id}">
										<input type = "submit" value = "コメント削除">（上記内容を削除します）
									</c:if>
									</form>
								</div>
						</c:if>
				</c:forEach>
				</div>
			</c:forEach>
		</div>


		<br/><div class = "copyright">Copyright(c)ogawa ayaka</div>





	</body>
</html>