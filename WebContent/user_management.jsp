<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
		<title>ユーザー管理画面</title>
	</head>
	<body>

	＜＜ユーザー管理画面＞＞<br/>

	<div class = "main-contents">

		<br/><a href = "signup">ユーザー新規登録</a><br/>


		<br/>＜＜ユーザー一覧＞＞

			<c:forEach items = "${users}" var = "user">

			<div class = "user">

				<br/>アカウント<div class = "account"><c:out value = "${user.account}"/></div>
				<br/>名前<div class = "name"><c:out value = "${user.name}"/></div>



				<div class = "branches">
				<c:forEach items = "${branches}" var= "branch">

					<c:if test = "${user.branch_id == branch.id}">
						<br/>支店名
						<div class = "name"><c:out value = "${branch.branch_name}"/></div>

					</c:if>

				</c:forEach>
				</div>

				<c:forEach items = "${departments}" var= "department">
				<c:if test = "${user.department_id == department.id}">
					<br/>部署
					<div class = "name"><c:out value = "${department.department_name}"/></div>
				</c:if>
				</c:forEach>


				<br/>ユーザー情報<div class = "is_stopped">
				<form action = "user_managements" method = "post">
					<c:if test = "${user.is_stopped == 1}">
						<input type = "submit" name = "button" value = "復活" onclick="return confirm('ユーザーを復活します。よろしいですか？')">
						<input name = "is_stopped" value = "0" type = "hidden">

					</c:if>

					<c:if test = "${user.is_stopped == 0 }">

						<input type = "submit" name = "button" value = "停止" onclick="return confirm('ユーザーを停止します。よろしいですか？')">
						<input name ="is_stopped" value = "1" type = "hidden">

					</c:if>

					<input name="id" value="${user.id}" id="id" type="hidden"/>



					 <c:out value = "${user.is_stopped}"/>

				</form>
					 </div>





				<br/>
				<a href = "user_settings?id=${user.id}">ユーザー編集</a>

			</div>

			</c:forEach>






			<a href = "./">戻る</a>

	</div>

	<div class = "copyright">Copyright(c)ogawa ayaka</div>



	</body>
</html>